## Tech Assessment Skeleton
This repo contains a bare skeleton for a Symmetric tech assessment. It is a dockerized single page web app featuring a Typescript/React frontend and Python/PostgreSQL backend.

### Development
To start the project, from the root of the repo run:

```bash
docker-compose up
```

This will start postgresql, pgadmin, and general development containers. The [entrypoint](docker-images/dev/entrypoint.sh) of the dev container  will:
 * Download some files from AccessGUDID, and insert them into the local PostgreSQL DB. The first time the container is started, the files will download (this takes a bit), printing `downloading gudid files...` in the console. Subsequent starts are cached.
 * Create a python virtualenv for the backend, and start a minimal flask app in development mode.
 * Yarn install the frontend, and start a create-react-app in development mode.

This startup is complete when the React frontend is running. Printing `You can now view frontend in the browser.` in the console.

If successful, you should be able to:
 * Navigate to http://localhost:3000/ to see the frontend (a create-react-app default screen).
 * Navigate to (or otherwise HTTP GET) http://localhost:8080/route and see a JSON array with a single empty object.
 * Navigate to http://localhost:5050 to see the pgAdmin browser. The connection info for PostgreSQL and pgAdmin can be found in [docker-compose.yml](docker-compose.yml). pgAdmin is not necessary if you have a preferred way of connecting to the PostgreSQL DB, it is provided for convenience.
 * Query the tables in the "tech" DB's "tech_user" schema. There should be 9 tables, each with data.

### Installing packages
The recommended way of installing packages is to execute commands from within the dev container. Examples listed below:
```bash
# backend

# open a prompt in the container
docker exec -it sym_dev bash  
# within this prompt
cd backend
# activate
source .venv/bin/activate
pip install <package>
```

```bash
# frontend

# open a prompt in the container
docker exec -it sym_dev bash  
# within this prompt
cd frontend
# activate
yarn add <package>
```