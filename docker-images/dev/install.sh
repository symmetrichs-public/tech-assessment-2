#!/usr/bin/env bash
set -e

apt-get update
apt-get install -y software-properties-common apt-transport-https curl zip unzip

add-apt-repository ppa:deadsnakes/ppa
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update

apt-get install -y python3.7 yarn python3-pip postgresql-client-11

echo "postgres:5432:*:tech_user:super_secret" > /root/.pgpass && chmod 0600 /root/.pgpass

rm /usr/bin/python3
ln -s /usr/bin/python3.7 /usr/bin/python3
ln -s /usr/bin/python3.7 /usr/bin/python
ln -s /usr/bin/pip3 /usr/bin/pip

yarn global add n && n 12.14
yarn cache clean

pip install --upgrade --no-cache-dir pip
pip install --no-cache-dir virtualenv

apt-get clean
rm -rf /var/lib/apt/lists/*
