#!/usr/bin/env bash

START_DIR=$PWD

# handle errors and assign local user file permissions on exit
err_handler() {
    CODE=$?
    echo "Error on line $1"
    echo $2
    cd $START_DIR
    find . -not -uid $(stat -c "%u" .) -exec chown --reference=. {} \;
    exit $CODE
}
#trap 'err_handler $LINENO' ERR

wait_for_db_ready() {
  set +e
  local CONNECTED=1
  while [ ! $CONNECTED == 0 ]; do
    echo "trying to connect..."
    timeout 5 psql -h postgres -U tech_user -c "show search_path;" tech
    CONNECTED=$?
    echo "CONNECTED=$CONNECTED"
    sleep 3
  done
  echo "Database ready!"
  set -e
}


#download GUDID data set if not already present
if [ ! -d /app/.gudid/data ]; then
  if [ ! -d /app/.gudid ]; then
    echo 'downloading gudid files...'
    mkdir /app/.gudid
    curl https://accessgudid.nlm.nih.gov/release_files/download/AccessGUDID_Delimited_Full_Release_20201001.zip --output /app/.gudid/full_release.zip
  fi
  unzip /app/.gudid/full_release.zip -d /app/.gudid/data
  cd $START_DIR
  find . -not -uid $(stat -c "%u" .) -exec chown --reference=. {} \;
fi

#copy GUDID data to postgresql
wait_for_db_ready
COMMENT=$(psql -h postgres -U tech_user -c "SELECT pg_catalog.obj_description(c.oid) FROM pg_namespace c WHERE c.nspname = 'tech_user';" tech)
INITIALIZED=$(echo $COMMENT | grep initialized || true)
if [ -z "$INITIALIZED" ]; then
  echo "importing db.."
  psql -h postgres -U tech_user -f "/db-init.sql" tech
fi


# create virtualenv and install backend, then start flask
cd backend
if [ ! -d .venv/bin ]; then
  python -m virtualenv .venv -p python3.7
fi
source .venv/bin/activate
pip install .[dev]
FLASK_APP=backend/api.py FLASK_ENV=development flask run --host 0.0.0.0 --port 8080 &
deactivate

# install js packages, then start frontend
cd ../frontend
yarn install
yarn start | cat - &

# monitor and update permissions for any files created within the docker container to match the host user id
cd $START_DIR
while [ 0 ]; do
  find . -not -uid $(stat -c "%u" .) -exec chown --reference=. {} \;
  sleep 5
done
