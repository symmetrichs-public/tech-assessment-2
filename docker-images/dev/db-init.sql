CREATE SCHEMA IF NOT EXISTS tech_user;

DROP TABLE IF EXISTS contact;
CREATE TABLE contact
(
    primary_di      text,
    phone           text,
    phone_extension text,
    email           text
);

COPY contact
    FROM '/app/.gudid/data/contacts.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS device;
CREATE TABLE device
(
    primary_di                        text,
    public_device_record_key          text,
    public_version_status             text,
    device_record_status              text,
    public_version_number             text,
    public_version_date               text,
    device_publish_date               text,
    device_comm_distribution_end_date text,
    device_comm_distribution_status   text,
    brand_name                        text,
    version_model_number              text,
    catalog_number                    text,
    duns_number                       text,
    company_name                      text,
    device_count                      text,
    device_description                text,
    dm_exempt                         text,
    premarket_exempt                  text,
    device_hctp                       text,
    device_kit                        text,
    device_combination_product        text,
    single_use                        text,
    lot_batch                         text,
    serial_number                     text,
    manufacturing_date                text,
    expiration_date                   text,
    donation_id_number                text,
    labeled_contains_nrl              text,
    labeled_no_nrl                    text,
    mri_safety_status                 text,
    rx                                text,
    otc                               text,
    device_sterile                    text,
    sterilization_prior_to_use        text
);

COPY device
    FROM '/app/.gudid/data/device.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS device_size;
CREATE TABLE device_size
(
    primary_di text,
    sizeType   text,
    size_unit  text,
    size_value text,
    size_text  text
);

COPY device_size
    FROM '/app/.gudid/data/deviceSizes.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS environmental_condition;
CREATE TABLE environmental_condition
(
    primary_di                              text,
    storage_handling_type                   text,
    storage_handling_high_unit              text,
    storage_handling_high_value             text,
    storage_handling_low_unit               text,
    storage_handling_low_value              text,
    storage_handling_special_condition_text text

);

COPY environmental_condition
    FROM '/app/.gudid/data/environmentalConditions.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS gmdn_term;
CREATE TABLE gmdn_term
(
    primary_di         text,
    gmdn_pt_name       text,
    gmdn_pt_definition text
);

COPY gmdn_term
    FROM '/app/.gudid/data/gmdnTerms.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS identifier;
CREATE TABLE identifier
(
    primary_di               text,
    device_id                text,
    device_id_type           text,
    device_id_issuing_agency text,
    contains_di_number       text,
    pkg_quantity             text,
    pkg_discontinue_date     text,
    pkg_status               text,
    pkg_type                 text
);

COPY identifier
    FROM '/app/.gudid/data/identifiers.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS premarket_submission;
CREATE TABLE premarket_submission
(
    primary_di        text,
    submission_number text,
    supplement_number text
);

COPY premarket_submission
    FROM '/app/.gudid/data/premarketSubmissions.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS product_code;
CREATE TABLE product_code
(
    primary_di        text,
    product_code      text,
    product_code_name text
);

COPY product_code
    FROM '/app/.gudid/data/productCodes.txt'
    WITH DELIMITER '|' HEADER CSV;

DROP TABLE IF EXISTS sterilization_method_type;
CREATE TABLE sterilization_method_type
(
    primary_di           text,
    sterilization_method text
);

COPY sterilization_method_type
    FROM '/app/.gudid/data/sterilizationMethodTypes.txt'
    WITH DELIMITER '|' HEADER CSV;

COMMENT ON SCHEMA tech_user IS 'initialized';
