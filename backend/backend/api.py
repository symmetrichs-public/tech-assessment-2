from flask import Flask, json
from flask_cors import CORS
from sqlalchemy import create_engine
import logging
import os

api = Flask(__name__)
CORS(api)

logger = logging.getLogger(__name__)

url = 'postgres' if os.getenv('RUNNING_IN_DOCKER') else 'localhost'
eng = create_engine(f'postgresql://tech_user:super_secret@{url}:5432/tech')


@api.route('/route', methods=['GET'])
def get_route():
    return json.dumps([{}])


def main():
    api.run()


if __name__ == '__main__':
    main()
